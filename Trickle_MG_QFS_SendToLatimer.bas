Attribute VB_Name = "Trickle_MG_QFS_SendToLatimer_Process"
Option Explicit

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "VDMS London Operations Department"
Const g_strFullUSerName = "VDMS London Operations"
Const g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=vie-sql-1.jcatv.co.uk;Failover_Partner=vie-sql-2.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'Const g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=VIE-TEST-SQL-1;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'Const g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=VIE-Z820-Tim;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

Public g_strUserEmailAddress As String
Public g_strUserEmailName As String
Public cnn As ADODB.Connection
Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Sub Main()

Dim rst As ADODB.Recordset, rstclip As ADODB.Recordset
Dim SQL As String, DaysPast As Integer, l_strCommandLine As String
Dim emailtext As String, ClipID As Long, TempDate As String, Channel As String
Dim EmailTo As String, EmailName As String
Dim NowDate As Date
Dim l_lngNewLibraryID As Long, l_lngNewEventID As Long, NumberOfItemsToSend As Long, CompanyID As Long
Dim TotalCount As Long, RunningCount As Long, Counter As Integer, Counter2 As Integer
Dim QueueCount As Long

Set cnn = New ADODB.Connection

SQL = g_strConnection

cnn.Open SQL

Set rst = New ADODB.Recordset
'Set rstclip = New ADODB.Recordset

SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strUserEmailAddress = rst("value")
End If
rst.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    g_strUserEmailName = rst("value")
End If
rst.Close

NumberOfItemsToSend = 10
CompanyID = 924
SQL = "SELECT Count(*) FROM vw_CopyQueue_QFS_Hotfioders;"
rst.Open SQL, cnn, 3, 3
If rst.RecordCount > 0 Then
    QueueCount = rst(0)
    rst.Close
    If QueueCount < NumberOfItemsToSend Then
    SQL = "SELECT TOP " & NumberOfItemsToSend & " eventID, bigfilesize, libraryID, companyID, md5checksum "
    SQL = SQL & "From vw_Events_on_DISCSTORE "
    SQL = SQL & "WHERE eventID NOT IN ( "
    SQL = SQL & "Select distinct DISCSTORE.eventID as [Disk Event] "
    SQL = SQL & "FROM ("
    SQL = SQL & "Select eventID, companyID, clipfilename, bigfilesize, md5checksum "
    SQL = SQL & "From vw_Events_on_DISCSTORE "
    SQL = SQL & "where vw_Events_on_DISCSTORE.system_deleted = 0) As DISCSTORE "
    SQL = SQL & "Join vw_Events_at_Latimer "
    SQL = SQL & "on vw_Events_at_Latimer.system_deleted = 0 "
    SQL = SQL & "AND vw_Events_at_Latimer.companyID = DISCSTORE.companyID "
    SQL = SQL & "AND vw_Events_at_Latimer.md5checksum = DISCSTORE.md5checksum "
    SQL = SQL & "Union "
    SQL = SQL & "Select distinct DIVASTORE.eventID as [Disk Event] "
    SQL = SQL & "from ("
    SQL = SQL & "Select eventID, companyID, clipfilename, bigfilesize, md5checksum "
    SQL = SQL & "From vw_Events_on_DIVA "
    SQL = SQL & "where vw_Events_on_DIVA.system_deleted = 0) As DIVASTORE "
    SQL = SQL & "Join vw_Events_at_Latimer "
    SQL = SQL & "on vw_Events_at_Latimer.system_deleted = 0 "
    SQL = SQL & "AND vw_Events_at_Latimer.companyID = DIVASTORE.companyID "
    SQL = SQL & "AND vw_Events_at_Latimer.md5checksum = DIVASTORE.md5checksum) "
    SQL = SQL & "AND (eventid in ("
    SQL = SQL & "Select distinct DISCSTORE.eventID as [Disk Event] "
    SQL = SQL & "from ("
    SQL = SQL & "Select eventID, companyID, clipfilename, bigfilesize "
    SQL = SQL & "From vw_Events_on_DISCSTORE "
    SQL = SQL & "Where vw_Events_on_DISCSTORE.system_deleted = 0 "
    SQL = SQL & "AND companyID = 924 "
    SQL = SQL & "AND libraryID = 726373 ) As DISCSTORE "
    SQL = SQL & "Join vw_Events_on_DIVA "
    SQL = SQL & "on vw_Events_on_DIVA.system_deleted = 0 "
    SQL = SQL & "AND vw_Events_on_DIVA.companyID = DISCSTORE.companyID "
    SQL = SQL & "AND vw_Events_on_DIVA.clipfilename = DISCSTORE.clipfilename "
    SQL = SQL & "AND vw_Events_on_DIVA.bigfilesize = DISCSTORE.bigfilesize "
    SQL = SQL & "AND vw_Events_on_DIVA.bigfilesize IS NOT NULL "
    SQL = SQL & "AND vw_Events_on_DIVA.DIVA_ID IS NOT NULL)) "
    SQL = SQL & "AND eventid in ("
    SQL = SQL & "select eventid from vw_BBCMG_ExportToLatimer) "
    SQL = SQL & "AND libraryID = 726373 "

    '    SQL = SQL & "and md5checksum IS NOT NULL "
    '    SQL = SQL & "and channelcount is not null " 'extra line
    '    SQL = SQL & "and channelcount <> 0 " 'extra line
    '    SQL = SQL & "and md5checksum <> '' " 'extra line
    '    SQL = SQL & "and clipformat ='MPEG 2 4:3' " 'extra line
    '    SQL = SQL & "and altlocation LIKE '905\Pathe%' " 'extra line
    '    SQL = SQL & "and libraryID = 726373; " 'extra line
        Debug.Print SQL
        rst.Open SQL, cnn, 3, 3
        If rst.RecordCount > 0 Then
            rst.MoveFirst
            TotalCount = rst.RecordCount
            RunningCount = 0
            Counter = 0
            Do While Not rst.EOF ' And Counter < 250
                RunningCount = RunningCount + 1
                Debug.Print "Item: " & RunningCount & " of " & TotalCount
                DoEvents
                l_lngNewLibraryID = GetData("library", "libraryID", "barcode", "LATIMER")
                l_lngNewEventID = CopyEventToLibraryID(Val(rst("eventID")), l_lngNewLibraryID, True)
                SetData "events", "sourceEventID", "eventID", l_lngNewEventID, rst("eventID")
                SetData "events", "sourcefrommedia", "eventID", l_lngNewEventID, 1
                SetData "events", "online", "eventID", l_lngNewEventID, 0
                If l_lngNewEventID <> 0 Then
                    'Invoke a copy to the hotfolder location
                    SQL = "INSERT INTO Event_File_Request (eventID, SourceLibraryID, Event_File_Request_TypeID, NewFileID, NewLibraryID, bigfilesize, RequestDate, RequestName, RequesterEmail) VALUES ("
                    SQL = SQL & rst("eventID") & ", "
                    SQL = SQL & rst("LibraryID") & ", "
                    SQL = SQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Export to Latimer") & ", "
                    SQL = SQL & l_lngNewEventID & ", " & l_lngNewLibraryID & ", "
                    SQL = SQL & rst("bigfilesize") & ", "
                    SQL = SQL & "'" & Format(DateAdd("D", 30, Now), "YYYY-MM-DD HH:NN:SS") & "', "
                    SQL = SQL & "'System', '" & g_strUserEmailAddress & "');"
                    Debug.Print SQL
                    cnn.Execute SQL
                    'Create a Webdelivery Entry
                    SQL = "INSERT INTO webdelivery (contactID, companyID, timewhen, clipID, bigfilesize, accesstype, finalstatus) VALUES ("
                    SQL = SQL & "3684, "
                    SQL = SQL & CompanyID & ", "
                    SQL = SQL & "'" & Format(Now, "YYYY-MM-DD hh:nn:ss") & "', "
                    SQL = SQL & rst("eventID") & ", "
                    SQL = SQL & rst("bigfilesize") & ", "
                    SQL = SQL & "'Export to Latimer Rd.', "
                    SQL = SQL & "'Completed')"
                    Debug.Print SQL
                    cnn.Execute SQL
                End If
                rst.MoveNext
                Counter = Counter + 1
            Loop
        End If
        rst.Close
    End If
End If

cnn.Close

Set rst = Nothing
Set cnn = Nothing

End Sub

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset

    l_rstGetData.Open l_strSQL, cnn, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing


    '<EhFooter>
    Exit Function

GetData_Err:
    MsgBox Err.Description & vbCrLf & _
           "in prjJCAFileManager.modTest.GetData " & _
           "at line " & Erl, _
           vbExclamation + vbOKOnly, "Application Error"
    Resume Next
    '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Public Function CopyEventToLibraryID(lp_lngEventID As Long, lp_lngLibraryID As Long, Optional lp_lngSkipLogging As Long) As Long

'copy the info from one job to another
Dim l_rstOriginalEvent As ADODB.Recordset
Dim l_rstNewEvent As ADODB.Recordset, l_lngNewEventID As Long
Dim c As ADODB.Connection

'get the original job's details
Dim l_strSQL As String

Set c = New ADODB.Connection
c.Open g_strConnection

l_strSQL = "INSERT INTO events ("
l_strSQL = l_strSQL & "eventmediatype, libraryID, companyID) VALUES ("
l_strSQL = l_strSQL & "'FILE', " & lp_lngLibraryID & ", 1);SELECT SCOPE_IDENTITY();"

Debug.Print l_strSQL
Set l_rstOriginalEvent = New ADODB.Recordset
Set l_rstOriginalEvent = c.Execute(l_strSQL)

l_lngNewEventID = l_rstOriginalEvent.NextRecordset().Fields(0).Value

'Record a History event
l_strSQL = "INSERT INTO eventhistory ("
l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
l_strSQL = l_strSQL & "'" & l_lngNewEventID & "', "
l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
l_strSQL = l_strSQL & "'Ceta', "
l_strSQL = l_strSQL & "'Created by Transcode' "
l_strSQL = l_strSQL & ");"

c.Execute l_strSQL

l_strSQL = "SELECT * FROM events WHERE eventID = '" & lp_lngEventID & "';"
l_rstOriginalEvent.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic

'allow adding of a new job
l_strSQL = "SELECT * FROM events WHERE eventID = '" & l_lngNewEventID & "';"
Set l_rstNewEvent = New ADODB.Recordset
l_rstNewEvent.Open l_strSQL, c, adOpenDynamic, adLockOptimistic

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
If l_rstOriginalEvent.EOF Then Exit Function
    
l_rstNewEvent("libraryID") = lp_lngLibraryID

For l_intLoop = 0 To l_rstOriginalEvent.Fields.Count - 1
    If l_rstOriginalEvent.Fields(l_intLoop).Name <> "libraryID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "eventID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "sound_stereo_russian" Then
    
        If Not IsNull(l_rstOriginalEvent(l_intLoop)) Then
            l_rstNewEvent(l_intLoop) = l_rstOriginalEvent(l_intLoop)
        End If
    
    End If
Next

'blank out fields which are specific to the ORIGINAL event only

l_rstNewEvent("cdate") = FormatSQLDate(Now)
l_rstNewEvent("cuser") = "CFM"

l_rstNewEvent("mdate") = FormatSQLDate(Now)
l_rstNewEvent("muser") = "CFM"

RESTART1:
On Error GoTo RESTART1
Err.Clear
l_rstNewEvent.Update
On Error GoTo 0

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalEvent.Close

'close the new record
l_rstNewEvent.Close
Set l_rstNewEvent = Nothing

'Copy the Segment Information re-using the l_rstOriginalEvent to read the records
l_rstOriginalEvent.Open "SELECT * FROM eventsegment WHERE eventID = " & lp_lngEventID, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO eventsegment (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("segmentreference")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestart") & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestop") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("note")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Wend
End If
l_rstOriginalEvent.Close

If lp_lngSkipLogging = 0 Then
    'Copy the Logging Information re-using the l_rstOriginalEvent to read the records
    l_rstOriginalEvent.Open "SELECT * FROM eventlogging WHERE eventID = " & lp_lngEventID, c, 3, 3
    If l_rstOriginalEvent.RecordCount > 0 Then
        l_rstOriginalEvent.MoveFirst
        While Not l_rstOriginalEvent.EOF
            l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ("
            l_strSQL = l_strSQL & l_lngNewEventID & ", "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("segmentreference")) & "', "
            l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestart") & "', "
            l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestop") & "', "
            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("note")) & "');"
            c.Execute l_strSQL
            l_rstOriginalEvent.MoveNext
        Wend
    End If
    l_rstOriginalEvent.Close
End If

'Copy the keyword Information
l_rstOriginalEvent.Open "SELECT * FROM eventkeyword WHERE eventID = " & lp_lngEventID, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO eventkeyword (eventID, keywordtext) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("keywordtext")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Wend
End If
l_rstOriginalEvent.Close

'Copy the iTunes Advisory Information
l_strSQL = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = " & lp_lngEventID & ";"
l_rstOriginalEvent.Open l_strSQL, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO iTunes_Clip_Advisory (eventID, advisorysystem, advisory) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("advisorysystem")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("advisory")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Wend
End If
l_rstOriginalEvent.Close

'Copy the iTunes Product Details
l_strSQL = "SELECT * FROM iTunes_product WHERE eventID = " & lp_lngEventID & ";"
l_rstOriginalEvent.Open l_strSQL, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO iTunes_product (eventID, territory, salesstartdate, clearedforsale) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("territory")) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_rstOriginalEvent("salesstartdate")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("clearedforsale")) & "');"
        Debug.Print l_strSQL
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Wend
End If
l_rstOriginalEvent.Close

Set l_rstOriginalEvent = Nothing

CopyEventToLibraryID = l_lngNewEventID

End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & lp_varValue & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & lp_varCriterea & "'"

Set c = New ADODB.Connection
c.Open g_strConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function


